interface ChunkRequest {

    lat: number;

    lon: number;

    height: number;
    
    radius?: number;
}

export default ChunkRequest;
