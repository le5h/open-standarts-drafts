enum AssetPriority {
    Initial, // before first draw (fonts, color correction, etc)

    Primary, // main content

    Secondary, // all other content (default)

    Optional, // can be skipped by viewer decision (low bandwidth, slow render, out of memory, far distance)
}

interface AssetMeta {
    key: string;
    
    value: any;
}

interface Asset {
    priority: AssetPriority;

    meta?: AssetMeta;

    files: string[];
}

export { AssetPriority, AssetMeta, Asset };
