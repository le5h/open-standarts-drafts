import Asset from './Asset';

interface ObjectMeta {
    key: string; // position, rotation, scale, title, description
    
    value: any; // string | number | number[]
}

interface ObjectPlacement {
    id?: string;

    meta: ObjectMeta;

    asset: Asset;
}

export default ObjectPlacement;
