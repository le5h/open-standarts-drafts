import ObjectPlacement from './ObjectPlacement';

interface ChunkResponse {
    objects: ObjectPlacement[];
}

export default ChunkResponse;
