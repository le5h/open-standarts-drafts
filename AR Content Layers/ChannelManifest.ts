interface ChannelManifets {
    name: string;

    description: string;

    icon: string;

    apiUrl: string;
}

export default ChannelManifets;
