import ThingInputField from './ThingInputField';

import ThingOutputField from './ThingOutputField';

class ThingPassport {
    uid: string; // MAC, UUID, Seri

    protocol: string; // IP

    address: string; // 192.168.0.128

    name: string;

    inputFields: ThingInputField[];

    outputFields: ThingOutputField[];

    // TODO: manufacturer info
}

export default ThingPassport;
