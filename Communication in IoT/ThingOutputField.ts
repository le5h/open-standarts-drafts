import FieldType from './FieldType';

interface ThingOutputField {
    key: string;

    type: FieldType;

    title?: string;

    description?: string;

    value: boolean | boolean[] | number | number[] | string | string[];
}

export default ThingOutputField;
