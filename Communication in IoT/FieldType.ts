enum FieldType {
    Toggle, // on, off
    Number, // temperature, speed
    Text, // pin code
    Range, // brightness
    Color, // any notation: #fff, rgb(255,255,255)
    Select, // days of week, mode
    Time, // at, from, to
}

export default FieldType;
