import FieldType from './FieldType';

class ThingInputField {
    key: string; // power, temperature, color

    type: FieldType;

    title?: string;

    description?: string;

    variants?: { [key: string]: string }; // { 'low': 'Minimum', 'high': 'Maximum' }

    defaultValue?: boolean | boolean[] | number | number[] | string | string[]; // [false, 0, '']

    value: boolean | boolean[] | number | number[] | string | string[];

    maxValues?: number; // 1, 2, 5
}

export default ThingInputField;
